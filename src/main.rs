use std::net::{SocketAddr, ToSocketAddrs};

use serde::{Serialize, Deserialize};

use duration_string::DurationString;

extern crate pxflib;

use pxflib::Pixels;

use clap::Parser;
use tokio::fs::File;
use tokio::io::AsyncReadExt;

/// Simple program to greet a person
#[derive(Parser, Debug, Clone)]
#[command(author, version, about, long_about)]
struct Args {
    host: Option<String>,
    file: Option<String>,
    #[arg(
        short,
        long,
        default_value_t = 1,
        long_help = "Set the Worker thread amount"
    )]
    thread_count: usize,
    #[arg(
        long,
        default_value_t = 0,
        long_help = "Set the Offset for the X position"
    )]
    offset_x: u32,
    #[arg(
        long,
        default_value_t = 0,
        long_help = "Set the Offset for the Y position"
    )]
    offset_y: u32,
    #[arg(
        long,
        default_value_t = 1280,
        long_help = "Set The Screen resolution on the X axis"
    )]
    screen_x: u32,
    #[arg(
        long,
        default_value_t = 720,
        long_help = "Set The Screen resolution on the Y axis"
    )]
    screen_y: u32,
    #[arg(long, long_help = "Resize the image at the X Axis")]
    resize_x: Option<u32>,
    #[arg(long, long_help = "Resize the image at the X Axis")]
    resize_y: Option<u32>,
    #[arg(
        long,
        long_help = "If Set a update thread will be created and updates the pixel offset each time defined here."
    )]
    hashed_duration: Option<String>,
    #[arg(long, long_help = "Defines a salt that are added to the hash")]
    salt: Option<String>,
    #[arg(
        long,
        short,
        default_value_t = false,
        long_help = "Enable dry Run, this means no tcp sessions are created and the Lines are printed to the console"
    )]
    dry_run: bool,
    #[arg(
        long,
        short,
        long_help = "Defines the location of the config file"
    )]
    config: Option<String>,
}

#[tokio::main]
async fn main() {
    let ts_subscriber = tracing_subscriber::fmt()
        .compact()
        .with_thread_ids(true)
        .finish();
    tracing::subscriber::set_global_default(ts_subscriber).unwrap();
    let args = Args::parse();
    let config : Config = match args.clone().config {
        Some(path) => {
            let mut file = File::open(path).await.expect("Can't open file");
            let mut buf = Vec::new();
            file.read_to_end(&mut buf).await.unwrap();
            serde_yaml::from_str(std::str::from_utf8(&buf).unwrap()).unwrap()

        },
        None => Config {
            addr: args.host.unwrap().to_socket_addrs().unwrap().next().unwrap(), 
            image_path: args.file.unwrap(),
            thread_count: args.thread_count,
            screen_size: (args.screen_x, args.screen_y),
            offset: (args.offset_x, args.offset_y),
            resize: if args.resize_x.is_some() && args.resize_y.is_some() {
                Some((args.resize_x.unwrap(), args.resize_y.unwrap()))
            } else {
                None
            },
            update_interval: args.hashed_duration, 
            salt: args.salt,
            dry_run: args.dry_run 
        },
    };
    let pixels = Pixels::new(
        config.image_path.as_str(),
        config.resize,
        config.screen_size,
        config.offset,
        config.update_interval.map(|str| DurationString::from_string(str).unwrap()),
        config.salt,
        config.addr,
    );

    pixels.start(args.thread_count, args.dry_run).await;
}

#[derive(Serialize, Deserialize)]
struct Config {
    addr: SocketAddr,
    image_path: String,
    thread_count: usize,
    screen_size: (u32,u32),
    offset: (u32, u32),
    resize: Option<(u32, u32)>,
    update_interval: Option<String>,
    salt: Option<String>,
    dry_run: bool
}
