use std::io::Write;
use std::net::{SocketAddr, TcpStream};

use std::sync::Arc;
use std::time::{Duration, SystemTime, UNIX_EPOCH};
use tokio::sync::RwLock;
use tokio::task::JoinHandle;

use blake2::{Blake2s256, Digest};
use duration_string::DurationString;
use image::{self, GenericImageView, Rgba};


#[derive(Debug, Clone)]
pub struct FloodPixel {
    x: u32,
    y: u32,
    offset: (u32, u32),
    rgba: image::Rgba<u8>,
    string: Option<String>,
}

impl FloodPixel {
    pub fn new(x: u32, y: u32, rgba: Rgba<u8>) -> Option<Self> {
        if rgba[3] <= 30 {
            return None;
        }

        let mut px = Self {
            x,
            y,
            rgba,
            offset: (0, 0),
            string: None,
        };
        px.update_string();
        Some(px)
    }
    fn update_string(&mut self) {
        let mut px_str = format!(
            "PX {} {} {:02X}{:02X}{:02X}",
            (self.x + self.offset.0),
            (self.y + self.offset.1),
            self.rgba[0],
            self.rgba[1],
            self.rgba[2]
        );
        if self.rgba[3] < 90 {
            px_str.push_str(&format!("{:02X}", self.rgba[3]));
        }
        px_str.push('\n');
        self.string = Some(px_str)
    }
    pub fn update_offset(&mut self, offset: (u32, u32)) {
        self.offset = offset;
        self.update_string();
    }
}

impl std::fmt::Display for FloodPixel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match &self.string {
            Some(string) => write!(f, "{}", string),
            None => write!(f, ""),
        }
    }
}

pub struct Pixels {
    pixels: Vec<Arc<RwLock<FloodPixel>>>,
    interval: Option<Duration>,
    screen_size: (u32, u32),
    resolution: (u32, u32),
    salt: String,
    addr: SocketAddr,
}

impl Pixels {
    pub fn new(
        file: &str,
        resize: Option<(u32, u32)>,
        screen_size: (u32, u32),
        offset: (u32, u32),
        hash_duration: Option<DurationString>,
        salt: Option<String>,
        addr: SocketAddr,
    ) -> Self {
        let mut tmp_img = image::open(file).expect("File Not Found");
        if resize.is_some() {
            let new_size = resize.expect("No resize values are supplied");
            tmp_img = tmp_img.thumbnail(new_size.0, new_size.1)
        }
        let dur: Option<Duration> = hash_duration.map(|dur| dur.into());
        let pixels: Vec<Arc<RwLock<FloodPixel>>> = tmp_img
            .pixels()
            .map(|(x, y, rgba)| FloodPixel::new(x, y, rgba))
            .filter(|x| x.is_some())
            .map(|x| {
                let mut item = x.unwrap();
                item.update_offset(offset);
                Arc::new(RwLock::new(item))
            })
            .collect();
        Self {
            pixels,
            interval: dur,
            salt: match salt {
                Some(salt) => salt,
                None => "".to_string(),
            },
            screen_size,
            resolution: (tmp_img.width(), tmp_img.height()),
            addr,
        }
    }

    async fn tcp_flood(&self, data: Vec<Arc<RwLock<FloodPixel>>>, addr:SocketAddr) -> JoinHandle<()> {
        tokio::spawn(async move {
            tracing::info!("Spawned new worker");
            let mut out: TcpStream = loop {
                match TcpStream::connect_timeout(&addr, Duration::new(5, 0)) {
                    Ok(r) => break r,
                    Err(er) => {
                        tracing::error! {%er, "failed to connect"}
                    }
                }
            };
            out.set_nodelay(true).expect("set_nodelay call failed");
            out.set_nonblocking(true)
                .expect("set_nonblocking call failed");
            loop {
                for pixel in &data {
                    let px = pixel.read().await;
                    for frame in px.to_string().as_bytes().chunks(20) {
                        let _ = out.write(frame);
                        let _ = out.flush();
                    }
                }
            }
        })

    }

    async fn debug_flood(&self, data: Vec<Arc<RwLock<FloodPixel>>>) -> JoinHandle<()> {
        tokio::spawn(async move {
                    tracing::info!("Spawned new worker");
                    loop {
                        if cfg!(debug_assertions) {
                            let pixel = &data[0];
                            {
                                let px = pixel.read().await;
                                tracing::info!("{}", px.to_string());
                            }
                        } else {
                            for pixel in &data {
                                let px = pixel.read().await;
                                for frame in px.to_string().as_bytes().chunks(20) {
                                    tracing::info!("{}", std::str::from_utf8(frame).unwrap());
                                }
                            }
                        }
                        tokio::time::sleep(Duration::new(1, 0)).await;
                    }
                })
    }

    async fn start_worker(&self, thread_count: usize, dry_run: bool) -> Vec<JoinHandle<()>> {
        let rwlock = self.pixels.clone();
        let addr = self.addr;
        let mut threads: Vec<JoinHandle<()>> = Vec::new();
        for chunk in rwlock.chunks(rwlock.len() / thread_count) {
            let data = chunk.to_owned();
            if dry_run {
                threads.push(self.debug_flood(data).await);
            } else {
                threads.push(self.tcp_flood(data, addr).await);
            }
        }
        threads
    }

    async fn start_updater(&self) -> tokio::task::JoinHandle<()> {
        let rwlock = self.pixels.clone();
        let interval = self.interval.unwrap();
        let salt = self.salt.clone();
        let resolution = self.resolution;
        let screen_size = self.screen_size;
        tokio::spawn(async move {
            let mut last_update: SystemTime;
            loop {
                last_update = SystemTime::now();
                let epoch: u64 = last_update.duration_since(UNIX_EPOCH).unwrap().as_secs();
                let to_be_hashed: String =
                    format!("{}{}", salt, (epoch / interval.as_secs()));
                let mut hasher = Blake2s256::new();
                hasher.update(to_be_hashed);
                let ret = hasher.finalize();
                let hash: [u8; 32] = ret.as_slice().try_into().expect("Wrong length");
                let new_offset: (u32, u32) = (
                    u32::from_be_bytes(hash[0..4].try_into().unwrap())
                        % (screen_size.0 - resolution.0),
                    u32::from_be_bytes(hash[4..8].try_into().unwrap())
                        % (screen_size.1 - resolution.1),
                );
                for px in &rwlock {
                    let mut w = px.write().await;
                    w.update_offset(new_offset);
                }
                tracing::info!(
                    "new Position offset(x: {}, y: {})",
                    new_offset.0,
                    new_offset.1
                );
                tokio::time::sleep(interval).await;
            }
        })
    }
    pub async fn start(&self, thread_count: usize, dry_run: bool) {
        let mut threads: Vec<JoinHandle<()>> = vec![];
        if self.interval.is_some() {
            threads.push(self.start_updater().await);
        }
        self.start_worker(thread_count, dry_run)
            .await
            .into_iter()
            .for_each(|i| threads.push(i));
        for handler in threads {
            handler.await.unwrap();
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use std::num::ParseIntError;
    use tokio::io::{AsyncBufReadExt, BufReader};
    use std::net::ToSocketAddrs;

    fn parse_pxl(string: String) -> FloodPixel {
        let splited_string: Vec<&str> = string.split(' ').collect();
        let rgba_vec: Result<Vec<u8>, ParseIntError> = (0..6)
            .step_by(2)
            .map(|i| u8::from_str_radix(&splited_string[3][i..i + 2].to_lowercase(), 16))
            .collect();
        let rgba_arr = rgba_vec.unwrap();
        let mut rgba: [u8; 4] = [0; 4];
        rgba[0] = rgba_arr[0];
        rgba[1] = rgba_arr[1];
        rgba[2] = rgba_arr[2];

        FloodPixel {
            x: splited_string[1].parse().unwrap(),
            y: splited_string[2].parse().unwrap(),
            offset: (0, 0),
            rgba: rgba.into(),
            string: None,
        }
    }

    #[test]
    fn flood_pixel() {
        let px1 = FloodPixel::new(0, 0, [255, 255, 255, 29].into());
        assert!(px1.is_none());
        let mut pixel = FloodPixel::new(0, 0, [255, 255, 255, 255].into()).unwrap();
        assert_eq!(pixel.to_string(), "PX 0 0 FFFFFF\n".to_string());
        pixel.update_offset((10, 10));
        assert_eq!(pixel.to_string(), "PX 10 10 FFFFFF\n".to_string());
        let px2 = FloodPixel::new(3, 4, [255, 255, 255, 64].into()).unwrap();
        assert_eq!(px2.to_string(), "PX 3 4 FFFFFF40\n");
    }

    #[tokio::test]
    async fn tcp_con() {
        let ts_subscriber = tracing_subscriber::fmt()
            .compact()
            .with_thread_ids(true)
            .finish();
        tracing::subscriber::set_global_default(ts_subscriber).unwrap();
        let pixels = Pixels::new(
            "test/test.png",
            None,
            (720, 1280),
            (0, 0),
            Some(DurationString::from_string("10s".to_string()).unwrap()),
            Some("test".to_string()),
            "127.0.0.1:1234".to_socket_addrs().unwrap().next().unwrap(),
        );
        tokio::spawn(async move {
            pixels.start(1, false).await;
        });
        let listner = tokio::net::TcpListener::bind("127.0.0.1:1234")
            .await
            .unwrap();
        println!("Strating tcp server");
        let (input, _) = listner.accept().await.unwrap();
        println!("new client");
        let mut output: Vec<FloodPixel> = vec![];
        let mut stream = BufReader::new(input);
        for _ in 0..100 {
            let mut line = String::new();
            stream.read_line(&mut line).await.unwrap();
            output.push(parse_pxl(line));
        }
        // test for offset position
        let test_px = &output[output.len() - 1];
        assert!(test_px.x > 10 || test_px.y > 10);
        // test for color values
        assert_eq!(output[0].rgba, [255, 0, 0, 0].into());
        assert_eq!(output[9].rgba, [0, 255, 0, 0].into());
        assert_eq!(output[90].rgba, [255, 255, 255, 0].into());
        assert_eq!(output[99].rgba, [0, 0, 255, 0].into());
        assert_eq!(output[44].rgba, [0, 0, 0, 0].into());
        assert_eq!(output[45].rgba, [0, 0, 0, 0].into());
        assert_eq!(output[54].rgba, [0, 0, 0, 0].into());
        assert_eq!(output[55].rgba, [0, 0, 0, 0].into());
    }
}
